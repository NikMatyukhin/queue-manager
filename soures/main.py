import sys
import form
from PyQt5 import QtWidgets, QtCore, QtGui

class QApplicationWindow(QtWidgets.QMainWindow, form.Ui_Widget):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle('Помощник в построении очередей')
        self.setWindowIcon(QtGui.QIcon(r'D:\PyProjects\очередник\icon.png'))
        
        self.currentList = []
        self.model       = QtCore.QStringListModel(self.currentList)
        self.studentList = ['Авсиевич Кирилл',
                            'Бардаханов Аркадий',
                            'Бородыня Максим',
                            'Бояршинин Кирилл',
                            'Гладько Иван',
                            'Говорин Кирилл',
                            'Дмитриев Вяеслав',
                            'Дойчев Владимир',
                            'Жеравин Филипп',
                            'Звягина Дарья',
                            'Зубкова Вероника',
                            'Ишеев Рабдан',
                            'Кац Оксана',
                            'Ковалева Татьяна',
                            'Коркин Максим',
                            'Лукина Дарья',
                            'Максимов Юрий',
                            'Матеров Иван',
                            'Меньшинин Андрей',
                            'Мерушкина Алина',
                            'Ненцинская Алёна',
                            'Родюшкин Владислав',
                            'Цыренжапов Аюр',
                            'Черненко Тимофей',
                            'Шпаков Станислав',
                            ]
        
        self.comboBox.addItems(self.studentList)
        self.listView.setModel(self.model)
        self.listView.SelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.pushButton_4.clicked.connect(self.add_student)
        self.pushButton.clicked.connect(self.push_up)
        self.pushButton_2.clicked.connect(self.put_down)
        self.pushButton_3.clicked.connect(self.drop)
        
    def add_student(self):
        if not self.comboBox.currentText() in self.currentList:
            self.currentList.append(self.comboBox.currentText())
            self.model.setStringList(self.currentList)
        
    def push_up(self):
        if self.listView.selectedIndexes():
            selectedIndex = self.listView.selectedIndexes()[0].row()
            self.currentList.insert(0, self.currentList.pop(selectedIndex))
            self.model.setStringList(self.currentList)
    
    def put_down(self):
        if self.currentList:
            self.currentList.append(self.currentList.pop(0))
            self.model.setStringList(self.currentList)
            
    def drop(self):
        if self.currentList:
            self.currentList.pop(0)
            self.model.setStringList(self.currentList)
            
            
if __name__ == '__main__':

    app = QtWidgets.QApplication(sys.argv)

    w = QApplicationWindow()
    w.show()

    sys.exit(app.exec_())
