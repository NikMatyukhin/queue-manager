# **ОЧЕРЕДНЯРА**

Я написал это приложение буквально на коленке, и я не буду его добарабывать по мере появления свободного времени.
Функционал приложения прост до ужаса:
1. Выберите пришедшего студента в выпадающем списке группы.
2. Нажатием на кнопку "Пришёл сдавать", поместите выбранного студента в список сдающих.
3. Если по какой-то причине студент должен пройти сдавать лабораторную вне очереди, нажмите "Пропущен вперёд".
4. Если студент облажался и он вынужден отправиться в конец очереди, нажмите "В конец очереди".
5. Если студент успешно защитил лабораторную, нажмите "Сдал, больше не сдаёт".
6. Если студент успешно защитил одну лабораторную, но хочет продолжать сдавать, отправьте его в конец очереди.

Думаю, разобраться самому, как работает это приложение, не составит труда. Но я всё же распишу.

![alt text](images/software.PNG "Ну просто же, ну?")
>>>
Общество знакомо с плохим дизайном лучше, чем с хорошим. 
Именно из-за этого люди предпочитают плохой дизайн хорошему. 
Они слишком сильно сжились с тем, что есть. 
Всё новое начинает пугать, а старое их утешает. 

(с) Paul Rand
>>>
Начну таки с формы - файл `form.py`. Саму форму собрал в QtDesigner ([читать](https://doc.qt.io/qt-5/qtdesigner-manual.html "Мануал по дизайнеру") и [скачать](https://build-system.fman.io/qt-designer-download "Верьте мне, я сам качал оттуда")), причём на оформление стиля одного единственного виджета я потратил больше времени, чем на всю форму целиком. Форма стандартная, никаких крутых наворотов не использовал, разве что `QListView` для более или менее красивого представления списка. В остальном же: четыре кнопки и комбо-бокс (выпадающий список). 

Сам код формы прикрепляю ниже, а пока что идеи для улучшений:
- [ ] добавить выпадающий список для выбора группы;
- [x] добавить доступ к элементам через выделение;
- [ ] исправить стили (а то выглядит стрёмно);
- [ ] наладить иконку (пропала, лол).

В коде, отвечающем за логику программы тоже нет ничего выдающегося. Дольше гуглил, как получить индекс выделенной ячейки, блин.

Сам код предлагаю посмотреть ниже, а вот идеи для доработок:
- [ ] хранить списки групп и студентов в базе данных;
- [ ] список студентов менять в зависимости от выбора в списке групп;
- [ ] добавить возможность `drag-and-drop`.

```python
class QApplicationWindow(QtWidgets.QMainWindow, form.Ui_Widget):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle('Помощник в построении очередей')
        self.setWindowIcon(QtGui.QIcon(r'D:\PyProjects\очередник\icon.png'))
        self.currentList = []
        self.model       = QtCore.QStringListModel(self.currentList)
        self.studentList = ['тут я сразу заполнил список, но мне стало стыдно и я стёр его из README']
        self.comboBox.addItems(self.studentList)
        self.listView.setModel(self.model)
        self.listView.SelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.pushButton_4.clicked.connect(self.add_student)
        self.pushButton.clicked.connect(self.push_up)
        self.pushButton_2.clicked.connect(self.put_down)
        self.pushButton_3.clicked.connect(self.drop)

    def add_student(self):
        if not self.comboBox.currentText() in self.currentList:
            self.currentList.append(self.comboBox.currentText())
            self.model.setStringList(self.currentList)

    def push_up(self):
        if self.listView.selectedIndexes():
            selectedIndex = self.listView.selectedIndexes()[0].row()
            self.currentList.insert(0, self.currentList.pop(selectedIndex))
            self.model.setStringList(self.currentList)

    def put_down(self):
        if self.currentList:
            self.currentList.append(self.currentList.pop(0))
            self.model.setStringList(self.currentList)

    def drop(self):
        if self.currentList:
            self.currentList.pop(0)
            self.model.setStringList(self.currentList)
```

